package internal

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.wikimedia.org/repos/cloud/toolforge/envvars-api/gen"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var envvarPrefix = "toolforge.envvar.v1."
var secretLabelSelector = "app=toolforge"

func fromK8sName(name string) string {
	return strings.ReplaceAll(strings.ToUpper(name[len(envvarPrefix):]), "-", "_")
}

func toK8sName(name string) (string, error) {
	if name != strings.ToUpper(name) {
		return "", fmt.Errorf("bad name passed: %s", name)
	}
	normalizedName := strings.ReplaceAll(strings.ToLower(string(name)), "_", "-")
	return fmt.Sprintf("%s%s", envvarPrefix, normalizedName), nil
}

func GetEnvvar(api EnvvarsApi, name string, namespace string) (int, interface{}) {
	k8sSecrets, err := api.Clients.K8s.CoreV1().Secrets(namespace).List(context.TODO(), metav1.ListOptions{
		LabelSelector: secretLabelSelector,
	})
	if err != nil {
		log.Errorf("GetEnvvar: Error trying to list secrets for namespace %s: %s", namespace, err)
		message := "Error contacting k8s cluster."
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	log.Debug(fmt.Sprintf("GetEnvvar: Got %d envvars!: %v", len(k8sSecrets.Items), k8sSecrets.Items))
	k8sName, err := toK8sName(name)
	// This should not happen, as there's validation at the middleware level by the openapi generated code (defined in the openapi yaml)
	if err != nil {
		message := fmt.Sprintf("Bad name for a secret: %s", name)
		log.Error(message)
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	for _, k8sSecret := range k8sSecrets.Items {
		if k8sSecret.Name != k8sName {
			log.Debug(fmt.Sprintf("GetEnvvar: Skipping secret as it does not match %s: %s", k8sName, k8sSecret.Name))
			continue
		}
		log.Debug(fmt.Sprintf("GetEnvvar: Got matching secret: %v", k8sSecret.Name))
		secretValue, found := k8sSecret.Data[name]
		if !found {
			log.Errorf("GetEnvvar: Unable to get secret, name %s not found in map %v", name, k8sSecret)
			message := "Bad secret internal format."
			return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
		}
		envvarValue := string(secretValue)
		return http.StatusOK, gen.GetResponse{
			Envvar:   &gen.Envvar{Name: name, Value: envvarValue},
			Messages: &gen.ResponseMessages{},
		}
	}
	message := fmt.Sprintf("Unable to find envvar with name '%s'", name)
	return http.StatusNotFound, gen.NotFound{Error: &[]string{message}}
}

func GetEnvvars(api EnvvarsApi, namespace string) (int, interface{}) {
	k8sSecrets, err := api.Clients.K8s.CoreV1().Secrets(namespace).List(context.TODO(), metav1.ListOptions{
		LabelSelector: secretLabelSelector,
	})
	if err != nil {
		log.Errorf("GetEnvvar: Error trying to list secrets for namespace %s: %s", namespace, err)
		message := "Error contacting k8s cluster."
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	log.Debugf("GetEnvvars: Got %d secrets! (namespace=%s)", len(k8sSecrets.Items), namespace)
	envvars := make([]gen.Envvar, 0)
	for _, k8sSecret := range k8sSecrets.Items {
		if !strings.HasPrefix(k8sSecret.Name, envvarPrefix) {
			log.Debugf("GetEnvvars: Skipping secret as it does not start with '%s': %s", envvarPrefix, k8sSecret.Name)
			continue
		}
		log.Debugf("GetEnvvars: Got matching secret: %v", k8sSecret.Name)
		secretName := fromK8sName(k8sSecret.Name)
		secretValue, found := k8sSecret.Data[secretName]
		if !found {
			log.Errorf("GetEnvvars: Unable to get secret, name %s not found in map %v", secretName, k8sSecret)
			message := "Bad secret internal format."
			return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
		}
		envvarValue := string(secretValue)
		envvars = append(envvars, gen.Envvar{Name: secretName, Value: envvarValue})
	}
	return http.StatusOK, gen.ListResponse{
		Envvars:  &envvars,
		Messages: &gen.ResponseMessages{},
	}
}

func CreateEnvvar(api EnvvarsApi, envvarName string, value string, namespace string) (int, interface{}) {
	log.Debugf("CreateEnvvar: creating envvar with name %s", envvarName)
	k8sName, err := toK8sName(envvarName)
	// This should not happen, as there's validation at the middleware level by the openapi generated code (defined in the openapi yaml)
	if err != nil {
		message := fmt.Sprintf("CreateEnvvar: Bad name for a secret: %s", envvarName)
		log.Error(message)
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	newK8sSecret, err := api.Clients.K8s.CoreV1().Secrets(namespace).Create(
		context.TODO(),
		&v1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Name: k8sName,
				Labels: map[string]string{
					"app":  "toolforge",
					"name": k8sName,
				},
			},
			Data: map[string][]byte{
				envvarName: []byte(value),
			},
		},
		metav1.CreateOptions{},
	)
	if err != nil {
		// A bit flaky way of handling, maybe improve in the future
		if strings.HasSuffix(err.Error(), "already exists") {
			newK8sSecret, err = api.Clients.K8s.CoreV1().Secrets(namespace).Update(
				context.TODO(),
				&v1.Secret{
					ObjectMeta: metav1.ObjectMeta{
						Name:   k8sName,
						Labels: map[string]string{"app": "toolforge"},
					},
					Data: map[string][]byte{
						envvarName: []byte(value),
					},
				},
				metav1.UpdateOptions{},
			)
			if err != nil {
				log.Errorf("CreateEnvvar: Error trying to update secret %s on namespace %s: %s", k8sName, namespace, err)
				message := "Error contacting k8s cluster."
				return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
			}
		} else if strings.Contains(err.Error(), "exceeded quota") {
			message := "Envvars quota exceeded"
			return http.StatusForbidden, gen.Forbidden{Error: &[]string{message}}
		} else {
			log.Errorf("CreateEnvvar: Error trying to create secret %s on namespace %s: %s", k8sName, namespace, err)
			message := "Error contacting k8s cluster."
			return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
		}
	}
	gottenName := fromK8sName(newK8sSecret.Name)
	gottenValue, found := newK8sSecret.Data[envvarName]
	if !found {
		log.Errorf("CreateEnvvar: Unable to get secret, name %s not found in map %v", gottenName, newK8sSecret)
		message := "Bad secret internal format."
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	stringValue := string(gottenValue)
	return http.StatusOK, gen.GetResponse{
		Envvar:   &gen.Envvar{Name: gottenName, Value: stringValue},
		Messages: &gen.ResponseMessages{},
	}
}

func DeleteEnvvar(api EnvvarsApi, secretName string, namespace string) (int, interface{}) {
	k8sEnvvars, err := api.Clients.K8s.CoreV1().Secrets(namespace).List(context.TODO(), metav1.ListOptions{
		LabelSelector: secretLabelSelector,
	})
	if err != nil {
		log.Errorf("DeleteEnvvar: Error trying to list secrets on namespace %s: %s", namespace, err)
		message := "Error contacting k8s cluster."
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	log.Debug(fmt.Sprintf("DeleteEnvvar: Got %d secrets!: %v", len(k8sEnvvars.Items), k8sEnvvars.Items))
	k8sName, err := toK8sName(secretName)
	// This should not happen, as there's validation at the middleware level by the generated code (defined in the openapi yaml)
	if err != nil {
		message := fmt.Sprintf("Bad name for a secret: %s", secretName)
		log.Error(message)
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	for _, k8sEnvvar := range k8sEnvvars.Items {
		if k8sEnvvar.Name != k8sName {
			log.Debug(fmt.Sprintf("DeleteEnvvar: Skipping secret as it does not match %s: %s", k8sName, k8sEnvvar.Name))
			continue
		}
		log.Debug(fmt.Sprintf("DeleteEnvvar: Got matching secret: %v", k8sEnvvar.Name))
		secretValue, found := k8sEnvvar.Data[secretName]
		if !found {
			log.Errorf("DeleteEnvvar: Unable to get secret, name %s not found in map %v", secretName, k8sEnvvar)
			message := "Bad secret internal format."
			return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
		}
		err := api.Clients.K8s.CoreV1().Secrets(namespace).Delete(context.TODO(), k8sName, metav1.DeleteOptions{})
		if err != nil {
			log.Errorf("DeleteEnvvar: Error trying to delete secret %s on namespace %s: %s", k8sName, namespace, err)
			message := "Error contacting k8s cluster."
			return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
		}
		valueString := string(secretValue)
		return http.StatusOK, gen.GetResponse{
			Envvar:   &gen.Envvar{Name: secretName, Value: valueString},
			Messages: &gen.ResponseMessages{},
		}
	}

	message := fmt.Sprintf("Unable to find secret with name '%s'", secretName)
	return http.StatusNotFound, gen.NotFound{Error: &[]string{message}}
}

func GetQuota(api EnvvarsApi, namespace string) (int, interface{}) {
	defaultErrorMessage := "Error contacting k8s cluster."
	k8sSecrets, err := api.Clients.K8s.CoreV1().Secrets(namespace).List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		log.Errorf("GetQuota: Error trying to get secrets for namespace %s: %s", namespace, err)
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{defaultErrorMessage}}
	}
	quotas, err := api.Clients.K8s.CoreV1().ResourceQuotas(namespace).List(context.TODO(), metav1.ListOptions{
		FieldSelector: fmt.Sprintf("metadata.name=%s", namespace),
	})
	if err != nil {
		log.Errorf("GetQuota: Error trying to get quotas for namespace %s: %s", namespace, err)
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{defaultErrorMessage}}
	}
	if len(quotas.Items) != 1 { // this is not supposed to happen
		message := fmt.Sprintf("Expected 1 quota, got %d", len(quotas.Items))
		log.Error(message)
		return http.StatusInternalServerError, gen.InternalError{Error: &[]string{message}}
	}
	log.Debugf("GetQuota: Got %d secrets! (namespace=%s)", len(k8sSecrets.Items), namespace)
	totalQuotaCount := quotas.Items[0].Spec.Hard[v1.ResourceSecrets]
	envvarsCount := 0
	othersCount := 0
	for _, k8sSecret := range k8sSecrets.Items {
		if strings.HasPrefix(k8sSecret.Name, envvarPrefix) &&
			k8sSecret.Labels["app"] == strings.Split(secretLabelSelector, "=")[1] {
			envvarsCount++
		} else {
			othersCount++
		}
	}
	log.Debugf("GetQuota: Got %d envvars and %d others", envvarsCount, othersCount)
	// subtract existing non-envvars secrets from total available secrets
	// quota to get total envvars quota
	envvarsQuotaCount := int(totalQuotaCount.Value()) - othersCount
	quota := gen.EnvvarsQuota{
		Quota: &envvarsQuotaCount,
		Used:  &envvarsCount,
	}
	return http.StatusOK, gen.QuotaResponse{
		Quota:    &quota,
		Messages: &gen.ResponseMessages{},
	}
}

func Healthcheck(api *EnvvarsApi) (int, gen.HealthResponse, error) {
	_, err := api.Clients.K8s.CoreV1().Secrets(api.Config.MyNamespace).List(
		context.TODO(),
		metav1.ListOptions{
			Limit: 1,
		},
	)
	if err != nil {
		message := fmt.Sprintf("Unable to contact the k8s API: %s", err)
		status := gen.HealthResponseStatus(gen.ERROR)
		return http.StatusInternalServerError, gen.HealthResponse{
			Messages: &gen.ResponseMessages{
				Info: &[]string{message},
			},
			Status: &status,
		}, nil
	}

	message := "All systems normal"
	status := gen.HealthResponseStatus(gen.OK)
	return http.StatusOK, gen.HealthResponse{
		Messages: &gen.ResponseMessages{
			Info: &[]string{message},
		},
		Status: &status,
	}, nil
}
