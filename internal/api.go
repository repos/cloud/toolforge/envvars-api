package internal

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.wikimedia.org/repos/cloud/toolforge/envvars-api/gen"
	"k8s.io/client-go/kubernetes"
)

type UserContextKey string

type UserContext struct {
	echo.Context
	User string
}

type Config struct {
	MyNamespace string
}

type Clients struct {
	K8s kubernetes.Interface
}

type EnvvarsApi struct {
	Clients        Clients
	Config         Config
	MetricsHandler echo.HandlerFunc
}

// Extract the tool name from the context, currently is the same user that authenticated to the api
func getToolFromContext(ctx echo.Context) string {
	uCtx := ctx.(UserContext)
	return uCtx.User
}

func enforceLoggedIn(ctx echo.Context) error {
	toolnameFromContext := getToolFromContext(ctx)
	if toolnameFromContext == "" {
		return fmt.Errorf("this endpoint needs authentication")
	}
	return nil
}

func getNamespaceForTool(toolnameFromContext string) string {
	return fmt.Sprintf("tool-%s", toolnameFromContext)
}

// Handlers
func (api EnvvarsApi) Show(ctx echo.Context, toolnameFromRequest string, id string) error {
	err := enforceLoggedIn(ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	toolnameFromContext := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolnameFromContext)

	code, response := GetEnvvar(api, id, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) List(ctx echo.Context, toolnameFromRequest string) error {
	err := enforceLoggedIn(ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	toolnameFromContext := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolnameFromContext)

	code, response := GetEnvvars(api, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Create(ctx echo.Context, toolnameFromRequest string) error {
	err := enforceLoggedIn(ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	toolnameFromContext := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolnameFromContext)

	var newEnvvar gen.Envvar
	err = (&echo.DefaultBinder{}).BindBody(ctx, &newEnvvar)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, fmt.Sprintf("Bad parameters passed: %s", err))
	}

	code, response := CreateEnvvar(
		api,
		newEnvvar.Name,
		newEnvvar.Value,
		toolNamespace,
	)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Delete(ctx echo.Context, toolnameFromRequest string, id string) error {
	err := enforceLoggedIn(ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	toolnameFromContext := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolnameFromContext)

	code, response := DeleteEnvvar(api, id, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Quota(ctx echo.Context, toolnameFromRequest string) error {
	err := enforceLoggedIn(ctx)
	if err != nil {
		return echo.NewHTTPError(http.StatusUnauthorized, "Unauthorized")
	}
	toolnameFromContext := getToolFromContext(ctx)
	toolNamespace := getNamespaceForTool(toolnameFromContext)

	code, response := GetQuota(api, toolNamespace)
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Healthcheck(ctx echo.Context) error {
	code, response, err := Healthcheck(&api)
	if err != nil {
		return err
	}
	return ctx.JSON(code, response)
}

func (api EnvvarsApi) Metrics(ctx echo.Context) error {
	return api.MetricsHandler(ctx)
}

func (api EnvvarsApi) Openapi(ctx echo.Context) error {
	swagger, err := gen.GetSwagger()
	if err != nil {
		return fmt.Errorf("error loading swagger spec\n: %s", err)
	}
	return ctx.JSON(http.StatusOK, swagger)
}
