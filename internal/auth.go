package internal

import (
	"fmt"
	"net/http"
)

func GetUserFromRequest(request *http.Request) (string, error) {
	// Currently we only care about the tool, not the user
	tool, ok := request.Header["X-Toolforge-Tool"]
	if !ok {
		return "", nil
	}
	if len(tool) != 1 {
		return "", fmt.Errorf("got an unexpected value for the tool authentication header: %v", tool)
	}
	return tool[0], nil
}
