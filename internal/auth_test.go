package internal

import (
	"net/http"
	"testing"
)

func TestGetUserFromRequestWorksWhenToolHeaderPassed(t *testing.T) {
	expectedUser := "dummy_user"
	gottenUser, err := GetUserFromRequest(&http.Request{
		Header: http.Header{"X-Toolforge-Tool": []string{expectedUser}},
	})

	if err != nil {
		t.Errorf("Expected no error, got %s", err)
	}

	if gottenUser != expectedUser {
		t.Errorf("Expected user %s, got %s", expectedUser, gottenUser)
	}
}

func TestGetUserFromRequestFailsWhenEmptyHeaderPassed(t *testing.T) {
	_, err := GetUserFromRequest(&http.Request{
		Header: http.Header{"X-Toolforge-Tool": []string{}},
	})

	if err == nil {
		t.Errorf("Expected an error, did not get any")
	}
}

func TestGetUserFromRequestFailsWhenManyValuesInHeaderPassed(t *testing.T) {
	_, err := GetUserFromRequest(&http.Request{
		Header: http.Header{"X-Toolforge-Tool": []string{"user_one", "user_two"}},
	})

	if err == nil {
		t.Errorf("Expected an error, did not get any")
	}
}

func TestGetUserFromRequestReturnsEmptyWhenNoHeaderPassed(t *testing.T) {
	expectedUser := ""
	gottenUser, err := GetUserFromRequest(&http.Request{
		Header: http.Header{},
	})

	if err != nil {
		t.Errorf("Expected no error, got %s", err)
	}

	if gottenUser != expectedUser {
		t.Errorf("Expected user %s, got %s", expectedUser, gottenUser)
	}
}
